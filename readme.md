<img alt="Logo" src="http://coderslab.pl/wp-content/themes/coderslab/svg/logo-coderslab.svg" width="400">

# Egzamin jQuery


1. Żeby zacząć, stwórz tak zwany [**fork**][forking] repozytorium z zadaniami.
2. Następnie [**sklonuj**][ref-clone] repozytorium na swój komputer.
3. Rozwiąż zadania i [**skomituj**][ref-commit] zmiany do swojego repozytorium.
4. [**Wypchnij**][ref-push] zmiany na swoje repozytorium na GitHubie.
5. [Stwórz **pull request**][pull-request] do oryginalnego repozytorium, gdy skończysz egzamin.

Na zadania tekstowe odpowiedz w plikach **txt**.
Resztę zadań rozwiąż w odpowiednich plikach **js**.

#### Zadanie 1
(2 pkt)

Czym jest jQuery? W jakich sytuacjach nie powinniśmy używać jQuery i dlaczego?

#### Zadanie 2
(2 pkt)

Opisz własnymi słowami, czym jest ```this```?
Jaka jest różnica między ```$(this)```, a ```this```?

#### Zadanie 3
(2 pkt)

Podaj adres strony lub aplikacji w sieci, która wyjątkowo Ci się podoba. Wymień przynajmniej trzy technologie lub biblioteki (oprócz HTML i CSS) wykorzystane przy jej tworzeniu.


#### Zadanie 4
(3 pkt)

Napisz kod, który spowoduje, że:

1. Po najechaniu myszką na **div** pojawi się  paragraf, który znajduje się wewnątrz tego diva
2. Po zjechaniu myszką z elementu **div** paragraf zniknie.

Powinien pojawiać się tylko ten paragraf, który znajduje się wewnątrz elementu **div**. Nie wszystkie paragrafy naraz.

#### Zadanie 5
(3 pkt)

Napisz kod, który spowoduje, że:

1. Po upłynięciu dwóch sekund **div** powiększy się dwukrotnie (animacja ma trwać jedną sekundę).
2. **Div** po powiększeniu ma zmienić swoje położenie o **100px** w lewo i **50px** w dół (animacja ma trwać pięć sekund).

#### Zadanie 6
(8 pkt)

Przeczytaj dokładnie polecenie, zanim weźmiesz się za pisanie kodu.

 Napisz kod, za pomocą którego sprawdzisz poprawność danych i prześlesz je pod odpowiedni adres.

 Warunki walidacji:
 
  1. Zarówno imię, jak i nazwisko muszą mieć co najmniej 3 znaki.

  Jeśli którekolwiek z nich ma mniej niż 3 znaki, wypisz w polu o klasie ```error-one``` informację: **"Zarówno imię, jak i nazwisko muszą mieć co najmniej 3 znaki"**.
  Zauważ, że element o tej klasie jest ukryty - pokaż go użytkownikowi.
  W przeciwnym przypadku wyczyść element o klasie ```error-one``` i ukryj go.

  2. Email musi zawierać znak **@** i musi mieć co najmniej 5 znaków (łącznie z @)

  Jeśli ten warunek jest nie spełniony dopisz do pola o klasie ```error-two``` informację: **"Email musi mieć co najmniej 5 znaków i zawierać znak @".**
  Zauważ, że element o tej klasie jest ukryty - pokaż go użytkownikowi.
  W przeciwnym przypadku wyczyść element o klasie ```error-two``` i ukryj go.

Jeśli wszsytkie wprowadzone dane są ok prześlij je pod adres "http://api.coderslab.pl/register" (method POST).
Pamiętaj, że dane powinny być przesłane jako zserializowany obiekt JSON. Przykładowe dane do wysłania:
  ```JSON
  {
  "name":"Jacek",
  "surname":"Tchorzewski",
  "address":"jacek.tchorzewski@cdrslab.pl"
  }
  ```
Jeżeli zapytanie się udało, serwer odeśle napis: "Rejestracja udana". Ukryj wtedy formularz, a zamiast niego pokaż  **div** , który znajduje się na samym dole ten z klasą ```alert-success``` z informacją o sukcesie. Jeżeli zapytanie się nie udało, serwer odeśle napis: "Złe dane". Wyświetl wtedy **div** o klasie ```alert-danger``` z komunikatem błędu przesłanym z serwera.



<!-- Links -->
[forking]: https://guides.github.com/activities/forking/
[ref-clone]: http://gitref.org/creating/#clone
[ref-commit]: http://gitref.org/basic/#commit
[ref-push]: http://gitref.org/remotes/#push
[pull-request]: https://help.github.com/articles/creating-a-pull-request
